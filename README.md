# Microservice Architecture

Car booking microservice architecture:

- Registration process: User information through 2FA authentication and push notification.
- Car booking: Booking create/cancel. Fare calculation and booking history.
- Notification: Booking, cancel and reminder operation.
- Car type management: Categorize vehicle type and renting policy.
- Cart management: Categorize vehicle and user history according.

# High Avaiable Microservices

 - Redundant ELB through HAProxy
 - Web Application Firewall (WAF) for filter requested api
 - App1, App2, App3 contains the microservices
 - S3 Bucket or Minio for storage
 - Master and read replica db for master-slave database

# Microservices Management

 - Jenkins for CI/CD pipeline deployment process
 - EFK for log analysis and application monitoring
 - Zabbix/CloudWatch for monitoring and alert
